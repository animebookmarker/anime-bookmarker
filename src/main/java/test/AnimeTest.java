package test;


import static org.junit.Assert.*;

import io.sky.anime.record.keeper.miscellaneous.UnconvertDynamoDBData;
import io.sky.anime.record.keeper.model.Anime;
import org.junit.Before;
import org.junit.After;
import org.junit.Test;


/**
 * Created by Trung on 7/11/2017.
 */
public class AnimeTest {

    private Anime anime;

    @Before
    public void setUp() throws Exception {
        this.anime = new Anime();
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testGetAndSetId() {
        final int id = 3;

        anime.setId(id);

        assertEquals(id, anime.getId());
    }

    @Test
    public void testGetAndSetTitleRaw() {
        final String title = "My Cool Anime";

        anime.setTitle(title);

        assertEquals(title, anime.getTitle());
    }

    @Test
    public void testGetAndSetEnglishTitle() {
        final String englishTitle = "My Eng Title";

        anime.setEnglishTitle(englishTitle);

        assertEquals(englishTitle, anime.getEnglishTitle());
    }

    @Test
    public void testGetAndSetEpisodes() {
        final int episodes = 33;

        anime.setEpisodes(episodes);

        assertEquals(episodes, anime.getEpisodes());
    }

    @Test
    public void testGetAndSetStatus() {
        final int airingStatus = 1;

        anime.setStatus(airingStatus);

        assertEquals(airingStatus, anime.getStatus());
    }

    @Test
    public void testSetStatusWithMyAnimeListTypeString() {
        final String currentlyAiring = "Currently Airing";
        final String finishedAiring = "Finished Airing";
        final String notYetAired = "Not yet aired";
        final String exceptionString = "blah";

        anime.setStatus(currentlyAiring);
        assertEquals(1, anime.getStatus());

        anime.setStatus(finishedAiring);
        assertEquals(2, anime.getStatus());

        anime.setStatus(notYetAired);
        assertEquals(3, anime.getStatus());

        anime.setStatus(exceptionString);
        assertEquals(2, anime.getStatus());
    }

    @Test
    public void testGetAndSetImageUrl() {
        final String imageUrl = "http://yayo.com";

        anime.setImageUrl(imageUrl);

        assertEquals(imageUrl, anime.getImageUrl());
    }
}
