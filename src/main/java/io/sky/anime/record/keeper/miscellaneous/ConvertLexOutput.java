package io.sky.anime.record.keeper.miscellaneous;

import org.springframework.stereotype.Service;

/**
 * Created by Trung on 7/14/2017.
 */
@Service
public class ConvertLexOutput {

    /**
     * Convert {SearchAnime=<SomeAnime>} to <SomeAnime>
     * @param s
     * @return
     */
    public String convert(String s){

        String output = "";

        for (int i = 0; i < s.length(); i++){

            char c = s.charAt(i);

            if (c == '='){
                i++;
                while (s.charAt(i) != '}'){
                    output += s.charAt(i);
                    i++;
                }
            }
        }
        // Return requested anime
        return output;
    }
}
