package io.sky.anime.record.keeper.connection;

import com.sun.org.apache.xerces.internal.impl.dv.util.Base64;

import io.sky.anime.record.keeper.miscellaneous.TextReader;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.ExecutionContext;
import org.apache.http.protocol.HTTP;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Trung on 6/25/2017.
 */
@Service
public class MyAnimeListConnection {

    // Text Reader
    private final TextReader textReader = new TextReader();
    private final String[] malAccount = textReader.databaseInfo(2);

    // Timeouts
    private static final int SOCKET_TIMEOUT = 5000;
    private static final int CONNECT_TIMEOUT = 3000;

    // MyAnimeList pre-set variables
    private String username;
    private String password;
    private String encodedAuthorization;
    DefaultHttpClient httpClient;
    BasicHttpContext localContext;

    /**
     * Used to request anime information from myanimelist.net when userdatabase requests to invoke API
     */
    public MyAnimeListConnection(){

        this.username = malAccount[0];
        this.password = malAccount[1];
        HttpParams httpParams = new BasicHttpParams();
        HttpConnectionParams.setConnectionTimeout(httpParams, CONNECT_TIMEOUT);
        HttpConnectionParams.setSoTimeout(httpParams, SOCKET_TIMEOUT);
        this.httpClient = new DefaultHttpClient(httpParams);

        String validation = this.username + ":" + this.password;
        this.encodedAuthorization = Base64.encode(validation.getBytes());

        // Setup localContext as BasicAuth
        this.localContext = new BasicHttpContext();
    }

    public String getEncodedAuthorization() {
        return encodedAuthorization;
    }

    /**
     * NOTE: FOR TESTING ONLY
     * Test user authentication
     * @return
     * @throws MyAnimeListException
     */
    public boolean authenticateMalUser() throws MyAnimeListException {

        HttpPost httpPost = new HttpPost("https://myanimelist.net/login.php");
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        boolean isAuthenticated;

        try {
            params.add(new BasicNameValuePair("username", this.username));
            params.add(new BasicNameValuePair("password", this.password));
            params.add(new BasicNameValuePair("cookies", "1"));
            UrlEncodedFormEntity ent = new UrlEncodedFormEntity(params, HTTP.UTF_8);

            httpPost.setEntity(ent);

            HttpResponse response = this.httpClient.execute(httpPost, this.localContext);
            HttpUriRequest currentReq = (HttpUriRequest) localContext.getAttribute(ExecutionContext.HTTP_REQUEST);
            HttpHost currentHost = (HttpHost)  localContext.getAttribute(ExecutionContext.HTTP_TARGET_HOST);
            String currentUrl = currentHost.toURI() + currentReq.getURI();
            HttpEntity entity = response.getEntity();

            if(entity != null) {
                entity.consumeContent();
            }

            isAuthenticated = currentUrl.equals("https://myanimelist.net/panel.php");
        } catch (Exception e) {
            throw new MyAnimeListException(e);
        }
        return isAuthenticated;
    }
}
