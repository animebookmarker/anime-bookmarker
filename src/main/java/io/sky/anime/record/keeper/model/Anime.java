package io.sky.anime.record.keeper.model;

/**
 * Created by Trung on 6/25/2017.
 */
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBDocument;

@DynamoDBDocument
public class Anime {

    private int aHashCode;
    private int id;
    private String title;
    private String englishTitle;
    private int episodes;
    private double score;
    private int status;
    private String start;
    private String end;
    private String imageUrl;
    private String followingStatus;

    // Airing types
    public static final int STATUS_CURRENTLY_AIRING = 1;
    public static final int STATUS_FINISHED_AIRING = 2;
    public static final int STATUS_NOT_YET_AIRED = 3;

    public Anime() {
        aHashCode = 0;
        id = 0;
        title = "";
        englishTitle = "";
        episodes = 0;
        score = 0.0;
        status = 0;
        start = null;
        end = null;
        imageUrl = "";
        followingStatus = "false";
    }

    @Override
    public int hashCode() {
        if(aHashCode == 0) {
            aHashCode = 17 * 37 + id;
            aHashCode = aHashCode * 17 + status;
            aHashCode = aHashCode * 17 + imageUrl.hashCode();
        }
        return aHashCode;
    }

    @Override
    public boolean equals(Object o) {
        if(!(o instanceof Anime))
            return false;
        Anime anime = (Anime) o;

        if(this.hashCode() == anime.hashCode())
            return true;
        return false;
    }

    public String getFollowingStatus() { return this.followingStatus; }

    public void setFollowingStatus(String watchStatus) { this.followingStatus = watchStatus; }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getEnglishTitle() {
        return englishTitle;
    }

    public void setEnglishTitle(String englishTitle) {
        this.englishTitle = englishTitle;
    }

    public int getEpisodes() {
        return this.episodes;
    }

    public void setEpisodes(int episodes) { this.episodes = episodes; }

    public double getScore() {
        return this.score;
    }

    public void setScore(double score) {
        this.score = score;
    }

    public int getStatus() {
        return this.status;
    }

    public String getStatusString() {
        switch(this.status) {
            case STATUS_CURRENTLY_AIRING: return "Currently Airing";
            case STATUS_FINISHED_AIRING: return "Finished Airing";
            case STATUS_NOT_YET_AIRED: return "Not yet aired";
            default: return "Finished Airing";
        }
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public void setStatus(String statusStr) {
        if(statusStr.equals("Currently Airing")) {
            this.status = STATUS_CURRENTLY_AIRING;
        } else if(statusStr.equals("Finished Airing")) {
            this.status = STATUS_FINISHED_AIRING;
        } else if(statusStr.equals("Not yet aired")) {
            this.status = STATUS_NOT_YET_AIRED;
        } else {
            this.status = STATUS_FINISHED_AIRING;
        }
    }

    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public String getEnd() {
        return end;
    }

    public void setEnd(String end) {
        this.end = end;
    }

    public String getImageUrl() {
        return this.imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

}
