package io.sky.anime.record.keeper.controller;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.lexruntime.AmazonLexRuntime;
import com.amazonaws.services.lexruntime.model.PostTextRequest;
import com.amazonaws.services.lexruntime.model.PostTextResult;
import io.sky.anime.record.keeper.connection.DynamoDBConnection;
import io.sky.anime.record.keeper.connection.LexConnection;
import io.sky.anime.record.keeper.connection.RDSConnection;
import io.sky.anime.record.keeper.miscellaneous.ConditionCheck;
import io.sky.anime.record.keeper.miscellaneous.ConvertLexOutput;
import io.sky.anime.record.keeper.model.Anime;
import io.sky.anime.record.keeper.security.PasswordService;
import io.sky.anime.record.keeper.security.SecurityService;
import io.sky.anime.record.keeper.service.AnimeSearchService;
import io.sky.anime.record.keeper.service.DynamoTableService;
import io.sky.anime.record.keeper.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;

/**
 * Created by Trung on 6/25/2017.
 */
@RestController
@RequestMapping("/api")
public class ApiController {

    @Autowired
    ConditionCheck conditionCheck;
    @Autowired
    UserService userService;
    @Autowired
    ConvertLexOutput convertLexOutput;
    @Autowired
    AnimeSearchService animeSearchService;
    @Autowired
    LexConnection lexConnection;
    @Autowired
    DynamoTableService dynamoTableService;
    @Autowired
    PasswordService passwordService;
    @Autowired
    SecurityService securityService;
    @Autowired
    RDSConnection rdsConnection;
    @Autowired
    DynamoDBConnection dynamoDBConnection;

    private Map<String, String> cache = new HashMap<>();

    /**
     * Create user
     * @param response
     * @param username
     * @param email
     * @param password
     * @return js file confirms success/failure
     */
    @CrossOrigin(origins = "http://localhost:3000")
    @RequestMapping(value = {"/signup"}, method = RequestMethod.POST)
    public synchronized HashMap<String, Object> createUser(
            HttpServletResponse response,
            @RequestParam(value="username") String username,
            @RequestParam(value="email") String email,
            @RequestParam(value="password") String password) throws Exception{

        // add user to cache
        cache.put(username, "noToken");

        HashMap<String, Object> js = new HashMap<>();
        HashMap<String, Object> errors = new HashMap<>();

        DynamoDBMapper mapper = dynamoDBConnection.connectDynamoDB();
        Connection sqlConnection = rdsConnection.connectSQL();

        // Check if user already exists
        if (!conditionCheck.userExist(username)){
            // Do createUser
            userService.createUser(mapper, sqlConnection, email, username, password);

            js.put("errors", errors);
            js.put("successMessage","success");

            response.setStatus(HttpServletResponse.SC_ACCEPTED);

            return js;
        }
        // Add failure to error query
        errors.put("username", username);
        errors.put("email", email);
        errors.put("password", password);
        errors.put("summary", "User already exists");

        // Add error map to js
        js.put("errors", errors);
        js.put("successMessage", "failure");

        return js;
    }

    /**
     * Login
     * @param username
     * @param password
     * @return
     */
    @CrossOrigin(origins = "http://localhost:3000")
    @RequestMapping(value = {"/login"}, method = RequestMethod.POST)
    public synchronized HashMap<String, Object> login(
            @RequestParam(value="username") String username,
            @RequestParam(value="password") String password){

        HashMap<String, Object> js = new HashMap<>();
        HashMap<String, Object> errors = new HashMap<>();

        Connection sqlConnection = rdsConnection.connectSQL();

        boolean isAuthenticated = securityService.authenticate(sqlConnection, username, password);

        // If username/password are correct
        if (isAuthenticated){
            // Use hashPassword function to generate BCrypt Hash
            String returnHash = passwordService.hashPassword(username);
            // Store user hash
            cache.put(username, returnHash);

            js.put("errors", errors);
            js.put("token", returnHash);
            js.put("successMessage", "success");

            return js;
        }

        // Otherwise return failure
        errors.put("username", username);
        errors.put("summary", "Authentication failed");

        js.put("errors", errors);

        return js;
    }

    /**
     * Add new anime to user's List<Anime> in DynamoDB using myAnimeList API
     * @param response
     * @param animeName
     * @param followingStatus
     * @param username
     * @return js file confirms success/failure
     * @throws Exception
     */
    @CrossOrigin(origins = "http://localhost:3000")
    @RequestMapping(value = {"/add/anime"}, method = RequestMethod.POST)
    public synchronized HashMap<String, Object> easyAdd(
            HttpServletResponse response,
            @RequestParam(value="anime_name") String animeName,
            @RequestParam(value="following_status") String followingStatus,
            @RequestParam(value="username") String username) throws Exception{

        animeName = animeName.split(",")[0];
        username = username.split(",")[0];
        followingStatus = followingStatus.split(",")[0];
        HashMap<String, Object> js = new HashMap<>();
        HashMap<String, Object> errors = new HashMap<>();

        DynamoDBMapper mapper = dynamoDBConnection.connectDynamoDB();

        // Check if anime is in user's table
        if (dynamoTableService.hasAnime(mapper, username, animeName)){
            // Add failure to js query
            errors.put("anime_name", animeName);
            errors.put("summary", "Anime already in user's list");

            js.put("errors", errors);

            return js;
        }

        // Do add anime
        userService.easyAdd(mapper, animeName, followingStatus, username);

        js.put("errors", errors);
        js.put("successMessage","success");
        response.setStatus(HttpServletResponse.SC_ACCEPTED);

        return js;
    }

    /**
     * Delete anime from user's List<Anime>
     * @param response
     * @param animeName
     * @param username
     * @return js file confirms success/failure
     * @throws Exception
     */
    @CrossOrigin(origins = "http://localhost:3000")
    @RequestMapping(value = {"/remove/anime"}, method = RequestMethod.POST)
    public synchronized HashMap<String, Object> easyRemove(
            HttpServletResponse response,
            @RequestParam(value="anime_name") String animeName,
            @RequestParam(value="username") String username) throws Exception{

        animeName = animeName.split(",")[0];
        username = username.split(",")[0];

        HashMap<String, Object> js = new HashMap<>();
        HashMap<String, Object> errors = new HashMap<>();

        DynamoDBMapper mapper = dynamoDBConnection.connectDynamoDB();

        // Check if anime is in user's table
        if (!dynamoTableService.hasAnime(mapper, username, animeName)){
            // Add failure to js query
            errors.put("summary", "Anime is not in user's list");

            js.put("errors", errors);
            return js;
        }

        // Do remove anime
        userService.easyRemove(mapper, animeName, username);

        js.put("errors", errors);
        js.put("removeAnime","success");
        response.setStatus(HttpServletResponse.SC_ACCEPTED);

        return js;
    }

    /**
     * Return data for requested anime
     * @param userRequest
     * @return
     */
    @CrossOrigin(origins = "http://localhost:3000")
    @RequestMapping(value = {"/information/anime"}, method = RequestMethod.GET)
    public synchronized HashMap<String, Object> getAnimeInformation(
            @RequestParam(value="user_request") String userRequest){

        String animeName;
        HashMap<String, Object> js = new HashMap<>();
        HashMap<String, Object> errors = new HashMap<>();
        HashMap<String, Object> anime = new HashMap<>();

        userRequest = userRequest.split(",")[0];
        /* Initialize Lex Chatbot */
        PostTextRequest textRequest = new PostTextRequest();
        AmazonLexRuntime client = lexConnection.connectLex();

        textRequest.setBotName("BookmarkerBot");
        textRequest.setBotAlias("bookmarkerbot");
        textRequest.setUserId("testuser");
        textRequest.setInputText(userRequest);
        /* Intialization ends here */

        try {
            PostTextResult textResult = client.postText(textRequest);
            String preConvertAnimeName = textResult.getSlots().toString();

            animeName = convertLexOutput.convert(preConvertAnimeName);
        } catch (NullPointerException e) {
            errors.put("summary", client.postText(textRequest).getMessage());
//            js.put("anime", anime);
            js.put("errors", errors);

            return js;
        }


        try {
            // If search results return nothing
            List<Anime> requestedAnimes = animeSearchService.searchAnime(animeName);

            if (requestedAnimes.size() == 0) {
                // Return failure js message
                errors.put("summary", "No anime with given name");
                js.put("anime", anime);
                js.put("errors", errors);
                return js;
            }
        } finally {
            // Otherwise send information
            Anime requestedAnime = animeSearchService.searchAnime(animeName).get(0);

            anime.put("anime_name", requestedAnime.getTitle());
            anime.put("total_ep", requestedAnime.getEpisodes());
            anime.put("start_date", requestedAnime.getStart());
            anime.put("end_date", requestedAnime.getEnd());
            anime.put("status", requestedAnime.getStatusString());
            anime.put("score", requestedAnime.getScore());
            anime.put("image_url", requestedAnime.getImageUrl());

            js.put("anime", anime);
            js.put("errors", errors);
            js.put("successMessage", "success");
            return js;
        }
    }

    /**
     * Return ArrayList<HashMap> where each hashmap is an instance of an anime in user's animeList
     * @param username
     * @return ArrayList<HashMap> in json
     * @throws Exception
     */
    @CrossOrigin(origins = "http://localhost:3000")
    @RequestMapping(value = {"/view"}, method = RequestMethod.GET)
    public synchronized HashMap<String, Object> viewUserAnimeList(
            @RequestParam(value="username") String username) throws Exception{
        username = username.split(",")[0];

        HashMap<String, Object> js = new HashMap<>();
        List<HashMap> outputList = new ArrayList<>();
        DynamoDBMapper mapper = dynamoDBConnection.connectDynamoDB();

        List<Anime> animeList = dynamoTableService.getUserAnimeList(mapper, username);

        for (Anime anime: animeList){
            // Create new hash map
            HashMap<String, Object> animeHashMap = new HashMap<>();

            System.out.println(anime.getEnd());
            // Input data into hash map
            animeHashMap.put("anime_name", anime.getTitle());
            animeHashMap.put("total_ep", anime.getEpisodes());
            animeHashMap.put("start_date", anime.getStart());
            animeHashMap.put("end_date", anime.getEnd());
            animeHashMap.put("status", anime.getStatusString());
            animeHashMap.put("score", anime.getScore());
            animeHashMap.put("following_status", anime.getFollowingStatus());
            animeHashMap.put("image_url", anime.getImageUrl());

            // Add hash map to list
            outputList.add(animeHashMap);
        }

        js.put("animes", outputList);
        js.put("successMessage","success");

        return js;
    }

    /**
     * Compare frontend and backend's tokens to check if user is authenticated
     * @param username
     * @param givenToken
     * @return
     */
    @CrossOrigin(origins = "http://localhost:3000")
    @RequestMapping(value = {"/dashboard"}, method = RequestMethod.GET)
    public synchronized HashMap<String, Object> compareToken(
            @RequestParam(value="username") String username,
            @RequestParam(value="token") String givenToken){

        HashMap<String, Object> js = new HashMap<>();
        HashMap<String, Object> errors = new HashMap<>();

        String currentToken = cache.get(username);
        if(!currentToken.equals(givenToken)){
            errors.put("summary", "Tokens not the same");
            js.put("errors", errors);
            return js;
        }
        System.out.println(currentToken);
        js.put("errors", errors);
        js.put("successMessage", "Tokens are the same");
        return js;
    }

    /**
     * Remove user's token when logout
     * @param username
     * @return
     */
    @CrossOrigin(origins = "http://localhost:3000")
    @RequestMapping(value = {"/logout"}, method = RequestMethod.GET)
    public synchronized HashMap<String, Object> logOut(
            @RequestParam(value="username") String username){

        // Update cache

        cache.put(username, "noToken");

        HashMap<String, Object> js = new HashMap<>();

        js.put("successMessage", "cache removed");
        return js;
    }
}