package io.sky.anime.record.keeper.service;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMappingException;
import io.sky.anime.record.keeper.model.Anime;
import io.sky.anime.record.keeper.model.DynamoTable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Trung on 7/12/2017.
 */
@Service
public class DynamoTableService {

    private final AnimeSearchService animeSearchService = new AnimeSearchService();

    /**
     * Update user's animeList
     * @param mapper table to retrieve target's list
     * @param targetUser to retrieve objects list
     * @return updated user's animeList
     */
    public List<Anime> updateAnimeList(DynamoDBMapper mapper, Anime anime, String targetUser){

        List<Anime> animeList;

        /* Retrieve current animeList list */
        animeList = getUserAnimeList(mapper, targetUser);

        /* Add to targetUser's shared model list if requested file is found */
        animeList.add(anime);

        return animeList;
    }

    /**
     * Retrieve user's sharedObject list
     * @param mapper table to retrieve list
     * @param username
     * @return user's sharedObject list
     */
    public List<Anime> getUserAnimeList(DynamoDBMapper mapper, String username){

        List<Anime> animeList;
        try {
        /* Retrieve current sharedObjects list */
            DynamoTable tableModel = mapper.load(DynamoTable.class, username);
            animeList = tableModel.getAnimeList();
        } catch (DynamoDBMappingException e){
        /* If blank shared list then return blank list */
            e.printStackTrace();
            animeList = new ArrayList<>();
        }

        return animeList;
    }

    /**
     * Confirm existence of requested Anime object in user's animeList
     * @param mapper
     * @param username
     * @param animeName
     * @return boolean
     */
    public boolean hasAnime(DynamoDBMapper mapper, String username, String animeName){

        if (animeSearchService.searchAnime(animeName).size() < 1){
            return false;
        }

        Anime targetAnime = animeSearchService.searchAnime(animeName).get(0);
        System.out.println(username);
        List<Anime> usersAnimeList = getUserAnimeList(mapper, username);

        for(Anime currentAnime: usersAnimeList){
            // Found target anime
            if (currentAnime.equals(targetAnime)){
                return true;
            }
        } return false;
    }
}
