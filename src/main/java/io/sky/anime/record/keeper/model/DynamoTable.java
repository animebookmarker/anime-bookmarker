package io.sky.anime.record.keeper.model;

import com.amazonaws.services.dynamodbv2.datamodeling.*;

import java.util.List;

/**
 * Created by Trung on 7/12/2017.
 */
@DynamoDBTable(tableName = "usersAnime")
public class DynamoTable {

    private String username;
    private List<Anime> animes;

    @DynamoDBHashKey(attributeName = "username")
    public String getUsername() {
        return this.username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @DynamoDBTypeConverted(converter = AnimeListConverter.class)
    @DynamoDBAttribute(attributeName = "animes")
    public List<Anime> getAnimeList() {
        return this.animes;
    }

    public void setAnimeList(final List<Anime> animes) {
        this.animes = animes;
    }
}

