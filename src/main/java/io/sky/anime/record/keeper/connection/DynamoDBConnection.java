package io.sky.anime.record.keeper.connection;

import com.amazonaws.AmazonClientException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDB;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import org.springframework.stereotype.Service;

/**
 * Created by Trung on 7/12/2017.
 */
@Service
public class DynamoDBConnection {

    private static DynamoDBMapper mapper;

    /**
     * Return dynamoTB table by reading credential files and given information
     * @throws Exception
     * @return AmazonDynamoDBClient model
     */
    public DynamoDBMapper connectDynamoDB() throws Exception{

        AWSCredentials credentials;
        AmazonDynamoDB client;

        try {
            credentials = new ProfileCredentialsProvider().getCredentials();
        } catch (Exception e){
            throw new AmazonClientException(
                    "Can't load credentials", e
            );
        }

        client = new AmazonDynamoDBClient(credentials);
        Region saEast1 = Region.getRegion(Regions.AP_SOUTHEAST_1);
        client.setRegion(saEast1);
        mapper = new DynamoDBMapper(client);

        return mapper;
    }
}
