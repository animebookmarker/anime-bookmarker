package io.sky.anime.record.keeper.miscellaneous;

import io.sky.anime.record.keeper.connection.RDSConnection;
import org.springframework.stereotype.Service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by Trung on 6/25/2017.
 */
@Service
public class ConditionCheck {

    private final RDSConnection rdsConnection = new RDSConnection();
    private final Connection conn = rdsConnection.connectSQL();

    /**
     * Check if username already exists
     * @param username
     * @return boolean false
     */
    public boolean userExist(String username){

        if (username == null) { return false; }

        String query = "SELECT * from users WHERE username = ?";
        try {
            PreparedStatement preparedStatement = conn.prepareStatement(query);
            preparedStatement.setString(1, username);

            // do update
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()){
                if (resultSet.getString("username").equals(username)){
                    return true;
                }
            }
            return false;
        } catch (SQLException e) {
            System.out.println("Can't check username");
        } return false;
    }
}

