package io.sky.anime.record.keeper;

import io.sky.anime.record.keeper.miscellaneous.ConvertLexOutput;
import io.sky.anime.record.keeper.miscellaneous.UnconvertDynamoDBData;
import io.sky.anime.record.keeper.model.Anime;
import io.sky.anime.record.keeper.service.AnimeSearchService;

import java.util.List;

/**
 * Created by Trung on 7/14/2017.
 */
public class MainTest {

    public static void main(String[] args){

        AnimeSearchService animeSearchService = new AnimeSearchService();
        Anime anime = animeSearchService.searchAnime("guilty crown").get(0);

        System.out.println(anime.getEpisodes());
    }
}
