package io.sky.anime.record.keeper.connection;

/**
 * Created by Trung on 6/25/2017.
 */
public class MyAnimeListException extends RuntimeException{

    private static final long serialVersionUID = 1L;
    private String additionalInfo = "";

    public MyAnimeListException(Exception e) {
        super(e);
        assert !(e instanceof MyAnimeListException) : e;
    }

    public MyAnimeListException(String string) {
        super(string);
    }

    public MyAnimeListException(String string, String additionalInfo) {
        this(string);
        this.setAdditionalInfo(additionalInfo);
    }

    public void setAdditionalInfo(String additionalInfo) {
        this.additionalInfo = additionalInfo;
    }

    public String getAdditionalInfo() {
        return additionalInfo;
    }

    public static class Timeout extends MyAnimeListException {
        private static final long serialVersionUID = 1L;

        public Timeout(String string) {
            super(string);
        }
    }

    public static class E50X extends MyAnimeListException {
        private static final long serialVersionUID = 1L;

        public E50X(String string) {
            super(string);
        }
    }

    public static class E403 extends MyAnimeListException {
        private static final long serialVersionUID = 1L;

        public E403(String string) {
            super(string);
        }
    }

    public static class E401 extends MyAnimeListException {
        private static final long serialVersionUID = 1L;

        public E401(String string) {
            super(string);
        }
    }

    public static class E404 extends MyAnimeListException {
        private static final long serialVersionUID = 1L;

        public E404(String string) {
            super(string);
        }
    }

    public static class RateLimit extends MyAnimeListException {
        private static final long serialVersionUID = 1L;

        public RateLimit(String string) {
            super(string);
        }
    }
}