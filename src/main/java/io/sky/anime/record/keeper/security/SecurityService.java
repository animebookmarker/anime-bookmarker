package io.sky.anime.record.keeper.security;

import org.springframework.stereotype.Service;

import java.sql.*;

/**
 * Created by Trung on 6/25/2017.
 */
@Service
public class SecurityService {

    private final PasswordService passwordService = new PasswordService();

    /**
     * Confirms correct username and password
     * @param connection database connection
     * @param username
     * @param password
     * @return boolean
     */
    public boolean authenticate(Connection connection, String username, String password) {

        String query = "select * from users where username  = ?";
        String passwordInDB;

        try {
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, username);

            // do update
            ResultSet rs = preparedStatement.executeQuery();
            if (rs.next()) {
                passwordInDB = rs.getString("password");
            } else {
                passwordInDB = null;
            }

            boolean isMatched = passwordService.checkPassword(password, passwordInDB);

            return isMatched;

        } catch (SQLException e) {
            System.out.println("Can't authenticate user");
        }

        return false;
    }

}
