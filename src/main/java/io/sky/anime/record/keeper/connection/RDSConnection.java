package io.sky.anime.record.keeper.connection;

import io.sky.anime.record.keeper.miscellaneous.*;
import org.springframework.stereotype.Service;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

/**
 * Created by Trung on 6/25/2017.
 */
@Service
public class RDSConnection {

    private static final TextReader reader = new TextReader();
    private static final String[] databaseInfo = reader.databaseInfo(1);

    private static final String DATABASE_DRIVER = databaseInfo[0];
    private static final String DATABASE_URL = databaseInfo[1];
    private static final String USERNAME = databaseInfo[2];
    private static final String PASSWORD = databaseInfo[3];

    // Initual connection
    private Connection connection;

    /**
     * Connect to database
     * @return
     */
    public Connection connectSQL() {

        if (connection == null) {
            try {
                Class.forName(DATABASE_DRIVER);
                connection = DriverManager.getConnection(DATABASE_URL, USERNAME, PASSWORD);
            } catch (ClassNotFoundException | SQLException e) {
                e.printStackTrace();
            }
        }
        return connection;
    }
}