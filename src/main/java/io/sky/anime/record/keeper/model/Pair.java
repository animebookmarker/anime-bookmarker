package io.sky.anime.record.keeper.model;

/**
 * Created by Trung on 7/14/2017.
 */
public class Pair {

    Anime anime;
    int indexString;

    public Pair(Anime anime, int indexString){
        this.anime = anime;
        this.indexString = indexString;
    }

    public Anime getAnime(){ return this.anime; }

    public int getIndex(){ return this.indexString; }
}
