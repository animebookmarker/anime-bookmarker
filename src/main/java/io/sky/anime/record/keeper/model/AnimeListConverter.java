package io.sky.anime.record.keeper.model;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTypeConverter;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import io.sky.anime.record.keeper.miscellaneous.UnconvertDynamoDBData;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Trung on 7/12/2017.
 */
public class AnimeListConverter implements DynamoDBTypeConverter<String, List<Anime>> {

    private static final ObjectMapper mapper = new ObjectMapper();
    private static final ObjectWriter writer = mapper.writerWithType(new TypeReference<List<Anime>>(){});
    private static final UnconvertDynamoDBData parse = new UnconvertDynamoDBData();

    @Override
    public String convert(List<Anime> obj) {
        try {
            return writer.writeValueAsString(obj);
        } catch (JsonProcessingException e) {
            System.out.println(
                    "Unable to marshall the instance of " + obj.getClass()
                            + "into a string");
            return null;
        }
    }

    @Override
    public List<Anime> unconvert(String s) {
        try {
            if (s.equals("[]")){
                return new ArrayList<Anime>();
            } else {
                List<Anime> list = parse.unconvertListData(s);
                return list;
            }
        } catch (Exception e) {
            System.out.println("Unable to unmarshall the string " + s
                            + "into " + s);
            return null;
        }
    }
}
