package io.sky.anime.record.keeper.miscellaneous;

import io.sky.anime.record.keeper.model.Anime;
import io.sky.anime.record.keeper.model.Pair;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Trung on 7/14/2017.
 */
public class UnconvertDynamoDBData {

    /**
     * Unconvert input s into List<Anime>
     * @param s
     * @return List<Anime>
     */
    public List<Anime> unconvertListData(String s){

        List<Anime> result = new ArrayList<>();
        String currentString = s;
        int currentIndex = 0;

        while (currentIndex <= s.length()-1 && currentString.length() >= 2){
            Pair parsedData = unconvertData(currentString);
            result.add(parsedData.getAnime());
            currentIndex = parsedData.getIndex();

            currentString = currentString.substring(currentIndex + 1);
        }

        return result;
    }

    /**
     * Unconvert input s into Anime object
     * @param s
     * @return Anime
     */
    public Pair unconvertData(String s){

        int jobQueueIndex = 0;
        int i;
        Anime thisAnime = new Anime();

        for (i = 0; i < s.length(); i++){

            char c = s.charAt(i);

            String toWrite = "";
            if (c == ':'){
                i++;
                while (s.charAt(i) != ','){
                    toWrite += s.charAt(i);
                    i++;
                    if (s.charAt(i) == '}'){
                        return new Pair(thisAnime, i);
                    }
                }
                jobQueue(jobQueueIndex, thisAnime, toWrite);
                jobQueueIndex++;
            }
        }
        return new Pair(thisAnime, i);
    }

    /**
     * Decide on function call based on current jobNumber
     * @param jobNumber
     * @param anime
     * @param queue
     */
    public void jobQueue(int jobNumber, Anime anime, String queue){

        String preParsedQueue;

        /* Parse out string quotation */
        try {
            preParsedQueue = queue.substring(1, queue.length() - 1);
        } catch (StringIndexOutOfBoundsException e){
            preParsedQueue = queue;
        }

        /* Decide on job */
        if (jobNumber == 0){
            anime.setId(Integer.parseInt(queue));
        } else if (jobNumber == 1){
            anime.setTitle(preParsedQueue);
        } else if (jobNumber == 2){
            anime.setEnglishTitle(preParsedQueue);
        } else if (jobNumber == 3){
            anime.setEpisodes(Integer.parseInt(queue));
        } else if (jobNumber == 4){
            anime.setScore(Double.parseDouble(queue));
        } else if (jobNumber == 5){
            anime.setStatus(Integer.parseInt(queue));
        } else if (jobNumber == 6){
            anime.setStart(preParsedQueue);
        } else if (jobNumber == 7){
            anime.setEnd(preParsedQueue);
        } else if (jobNumber == 8){
            anime.setImageUrl(preParsedQueue);
        } else if (jobNumber == 9){
            anime.setFollowingStatus(preParsedQueue);
        } else{
            anime.setStatus(preParsedQueue);
        }
    }
}
