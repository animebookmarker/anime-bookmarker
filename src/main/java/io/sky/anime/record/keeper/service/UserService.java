package io.sky.anime.record.keeper.service;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import io.sky.anime.record.keeper.model.Anime;
import io.sky.anime.record.keeper.model.DynamoTable;
import io.sky.anime.record.keeper.security.PasswordService;
import org.springframework.stereotype.Service;

/**
 * Created by Trung on 6/25/2017.
 */
@Service
public class UserService {

    private final PasswordService passwordService = new PasswordService();
    private final DynamoTableService dynamoTableService = new DynamoTableService();
    private final AnimeSearchService animeSearchService = new AnimeSearchService();

    /**
     * Create user
     * @param rdsConnection database connection
     * @param email
     * @param password
     */
    public void createUser(DynamoDBMapper mapper, Connection rdsConnection, String email, String username, String password){

        // Hash input password
        String hashedPassword = passwordService.hashPassword(password);

        String query = "insert into users (username, email, password)"
                + " values (?, ?, ?)";
        try {
            PreparedStatement preparedStatement = rdsConnection.prepareStatement(query);
            preparedStatement.setString(1, username);
            preparedStatement.setString(2, email);
            preparedStatement.setString(3, hashedPassword);

            /* Do RDS database update */
            preparedStatement.executeUpdate();

            /* Create blank shared model list for new user in dynamoDB */
            List<Anime> blankList = new ArrayList<>();

            DynamoTable tableModel = new DynamoTable();
            tableModel.setUsername(username);
            tableModel.setAnimeList(blankList);

            mapper.save(tableModel);

        } catch (SQLException e) {
            System.out.println("Can't create user" + e);
        }
    }

    /**
     * Add new anime to user's List<Anime> in DynamoDB using myAnimeList API
     * @param mapper
     * @param animeName
     * @param email
     */
    public void easyAdd(DynamoDBMapper mapper, String animeName, String followingStatus, String email){
        System.out.println(email);
        System.out.println(animeName);
        if (animeSearchService.searchAnime(animeName).size() < 1){
            System.out.println("HELLO");
            return;
        }
        // Always take first search outcome for now
        Anime anime = animeSearchService.searchAnime(animeName).get(0);
        anime.setFollowingStatus(followingStatus);

        // Retrieve and update target user's shared model list
        List<Anime> animeList = dynamoTableService
                .updateAnimeList(mapper, anime, email);

        // Do table update
        DynamoTable tableModel = new DynamoTable();
        tableModel.setAnimeList(animeList);
        tableModel.setUsername(email);
        mapper.save(tableModel);
    }

    /**
     * Delete anime from user's List<Anime>
     * @param mapper
     * @param animeName
     * @param email
     */
    public void easyRemove(DynamoDBMapper mapper, String animeName, String email){

        List<Anime> animeList = dynamoTableService.getUserAnimeList(mapper, email);
        Anime targetAnime = animeSearchService.searchAnime(animeName).get(0);
        String targetAnimeTitle = targetAnime.getTitle();

        // Copy all Anime objects into new list excluding anime that is requested for removal
        List<Anime> resultList = new ArrayList<>();

        for (Anime currentAnime: animeList){
            // Do not copy anime that is requested for remove
            if (!currentAnime.getTitle().equals(targetAnimeTitle)){
                resultList.add(currentAnime);
            }
        }

        // Do table update
        DynamoTable tableModel = new DynamoTable();
        tableModel.setAnimeList(resultList);
        tableModel.setUsername(email);
        mapper.save(tableModel);
    }
}