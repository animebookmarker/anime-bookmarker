package io.sky.anime.record.keeper.connection;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.lexruntime.AmazonLexRuntime;
import com.amazonaws.services.lexruntime.AmazonLexRuntimeClientBuilder;
import org.springframework.stereotype.Service;

/**
 * Created by Trung on 7/14/2017.
 */
@Service
public class LexConnection {

    /**
     * Connect to Lex Chatbot
     * @return client
     */
    public AmazonLexRuntime connectLex(){

        AmazonLexRuntime client = AmazonLexRuntimeClientBuilder.standard()
                .withRegion(Regions.US_EAST_1).build();

        return client;
    }
}
