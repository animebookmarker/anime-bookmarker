package io.sky.anime.record.keeper.miscellaneous;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

/**
 * Created by Trung on 6/25/2017.
 */
public class TextReader {

    private final String DATABASE_FILENAME = "databaseInfo.txt";
    private final String MAL_ACCOUNT_FILENAME = "myAnimeListAccount.txt";

    /**
     * Read from the databaseInfo.txt file for database connection properties
     * @return String array of properties
     */
    public String[] databaseInfo(int type) {

        String[] info = new String[4];
        String FILENAME;

        if (type == 1){
            FILENAME = DATABASE_FILENAME;
        } else {
            FILENAME = MAL_ACCOUNT_FILENAME;
        }

        try (BufferedReader br = new BufferedReader(new FileReader(FILENAME))) {

            String currentLine;
            int i = 0;

            while ((currentLine = br.readLine()) != null) {
                info[i] = currentLine;
                i++;
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        return info;
    }
}
