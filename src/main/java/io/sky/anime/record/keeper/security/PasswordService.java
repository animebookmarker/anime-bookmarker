package io.sky.anime.record.keeper.security;

import org.mindrot.jbcrypt.*;
import org.springframework.stereotype.Service;

/**
 * Created by Trung on 6/25/2017.
 */
@Service
public class PasswordService {

    /**
     * Hash input password into BINARY(60) hashed/salted data type
     * @param password
     * @return
     */
    public String hashPassword(String password){ return BCrypt.hashpw(password, BCrypt.gensalt()); }

    /**
     * Check input password and hashed password in database
     * @param candidate
     * @param hashed
     * @return
     */
    public boolean checkPassword(String candidate, String hashed){ return BCrypt.checkpw(candidate, hashed); }

}
