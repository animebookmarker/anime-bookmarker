package io.sky.anime.record.keeper;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class RecordKeeperApp {

    public static void main(String[] args) {
        SpringApplication.run(RecordKeeperApp.class, args);
    }
}