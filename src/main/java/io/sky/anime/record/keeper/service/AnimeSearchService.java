package io.sky.anime.record.keeper.service;

import io.sky.anime.record.keeper.connection.MyAnimeListConnection;
import io.sky.anime.record.keeper.connection.MyAnimeListException;
import io.sky.anime.record.keeper.model.Anime;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLEncoder;
import java.sql.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import javax.net.ssl.HttpsURLConnection;

/**
 * Created by Trung on 6/25/2017.
 */
@Service
public class AnimeSearchService {

    private final int CONNECT_TIMEOUT = 3000;
    private final MyAnimeListConnection myAnimeListConnection = new MyAnimeListConnection();
    private final DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");

    /**
     * Request anime search to myanimelist.net
     * @param query
     * @return Array List of Anime objects
     * @throws MyAnimeListException
     */
    public ArrayList<Anime> searchAnime(String query) throws MyAnimeListException {

        ArrayList<Anime> searchResults = new ArrayList<Anime>();
        String encodedQuery;

        try {
            encodedQuery = URLEncoder.encode(query, "UTF-8");
            URL url = new URL("https://myanimelist.net/api/anime/search.xml?q=" + encodedQuery);
            HttpsURLConnection con = (HttpsURLConnection) url.openConnection();
            con.setRequestMethod("GET");
            con.setRequestProperty("Authorization", "Basic "+ myAnimeListConnection.getEncodedAuthorization());
            con.setConnectTimeout(CONNECT_TIMEOUT);
            InputStream inputStream = con.getInputStream();

            if(con.getResponseCode() == HttpsURLConnection.HTTP_OK) {
                searchResults = parseXMLResults(inputStream);
            }

        } catch (Exception e) {
            throw new MyAnimeListException(e);
        }

        return searchResults;
    }

    /**
     * Parse XML Results from search request to myanimelist.net
     * @param inputXML
     * @return Array List of Anime objects
     * @throws MyAnimeListException
     * @throws IOException
     * @throws JDOMException
     */
    public ArrayList<Anime> parseXMLResults(InputStream inputXML) throws MyAnimeListException, IOException, JDOMException {

        // Entry info tags
        final String ENTRY = "entry";
        final String ID = "id";
        final String TITLE = "title";
        final String ENGLISH = "english";
        final String SYNONYMS = "synonyms";
        final String EPISODES = "episodes";
        final String SCORE = "score";
        final String STATUS = "status";
        final String START_DATE = "start_date";
        final String END_DATE = "end_date";
        final String IMAGE = "image";

        final ArrayList<Anime> searchResults = new ArrayList<>();

        // XML nodes to detect
        SAXBuilder builder = new SAXBuilder();
        Document document = (Document) builder.build(inputXML);

        // Build root node
        Element rootNode = document.getRootElement();
        List subRootNodes = rootNode.getChildren(ENTRY);

        for (int i = 0; i < subRootNodes.size(); i++){

            Anime currentAnime = new Anime();
            Element currentNode = (Element) subRootNodes.get(i);

            // Retrieve children
            int id = Integer.parseInt(currentNode.getChildText(ID));
            String title = currentNode.getChildText(TITLE);
            String englishTitle = currentNode.getChildText(ENGLISH);
            int episodes = Integer.parseInt(currentNode.getChildText(EPISODES));
            Double score = Double.parseDouble(currentNode.getChildText(SCORE));
            String status = currentNode.getChildText(STATUS);
            Date startDate = Date.valueOf(currentNode.getChildText(START_DATE));
            Date endDate = Date.valueOf(currentNode.getChildText(END_DATE));
            String imageURL = currentNode.getChildText(IMAGE);
            String[] synonyms = new String[0];

            // Special case for synonyms
            if (!currentNode.getChildText(SYNONYMS).equals("")) {
                synonyms = currentNode.getChildText(SYNONYMS).split(";\\s");
            }

            // Set currentAnime's variables
            currentAnime.setId(id);
            currentAnime.setTitle(title);
            currentAnime.setEnglishTitle(englishTitle);
            currentAnime.setEpisodes(episodes);
            currentAnime.setScore(score);
            currentAnime.setStatus(status);
            currentAnime.setStart(dateFormat.format(startDate));
            currentAnime.setEnd(dateFormat.format(endDate));
            currentAnime.setImageUrl(imageURL);

            // Add anime to final array output
            searchResults.add(currentAnime);
        }

        return searchResults;
    }

}
